package com.trzy.it.papersoccer.protocol;

import com.trzy.it.papersoccer.multiplayer.socket.game.model.Player;
import com.trzy.it.papersoccer.multiplayer.socket.game.model.Table;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.trzy.it.papersoccer.MultiplayerSocketGameActivity;
import com.trzy.it.papersoccer.multiplayer.socket.game.Arrays;


public class ParseMessage {
	
	//Typy błędów
	private static final int ERROR_WRONG_PACKAGE		= 1; //Nieprawidłowy typ pakiety lub błęny format pakietu.
	private static final int ERROR_LOGIN_NOT_AVAILABLE	= 2; //Logowanie - nick zajęty.
	private static final int ERROR_NO_FREE_PLACES		= 3; //Logowanie - wszystkie miejsca zajęte.
	private static final int ERROR_LOGIN_FORBIDDEN		= 4; //Logowanie - niedozwolony nick
	private static final int ERROR_TABLE_NOT_EXIST		= 5; //Stolik Logowanie - Stolik nie istnieje.
	private static final int ERROR_TABLE_FULL			= 6; //Stolik Logowanie - Obydwa miejsca przy stoliku zajęte.
	private static final int ERROR_MAX_TABLES			= 7; //Stolik Tworzenie - Nie można utworzyć więcej stolików.
	private static final int ERROR_NOT_USER_TURN		= 8; //Nie Twój ruch.
	private static final int ERROR_WRONG_TURN			= 9; //Nieprawidłowy ruch.
	
	//Typy pakietów
	private static final int PACKAGE_DATA_ERROR			= 1;
	private static final int PACKAGE_DATA_LOGIN_SUCCESS	= 2;
	private static final int PACKAGE_DATA_TABLES_LIST	= 3;
	private static final int PACKAGE_DATA_TABLE_JOIN	= 4;
	private static final int PACKAGE_DATA_PLAYERS_LIST	= 5;
	private static final int PACKAGE_DATA_PLAYER_STATUS	= 6;
	private static final int PACKAGE_DATA_TABLE_LEAVE	= 7;
	private static final int PACKAGE_DATA_TABLE_STATUS	= 8;
	private static final int PACKAGE_DATA_GAME_READY	= 9;
	
	private static JSONObject jsonObject;
	
	public static String message;
	public static Integer TYPE;
	public static Integer ERROR;
	
	public static void parse() throws JSONException {
		try {
			jsonObject = new JSONObject(message);
			TYPE = Integer.valueOf(jsonObject.getString("type"));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		switch(TYPE) {
			case PACKAGE_DATA_ERROR:
				ParseMessage.onError();
				break;
			case PACKAGE_DATA_LOGIN_SUCCESS:
				ParseMessage.onSuccessLogin();
				break;
			case PACKAGE_DATA_TABLES_LIST:
				ParseMessage.onTableList(jsonObject);
				break;
			case PACKAGE_DATA_TABLE_JOIN:
				ParseMessage.onTableJoin(jsonObject);
				break;
			case PACKAGE_DATA_PLAYERS_LIST:
				ParseMessage.onPlayerList(jsonObject);
				break;
			case PACKAGE_DATA_PLAYER_STATUS:
				ParseMessage.onPlayerUpdate(jsonObject);
				break;
			case PACKAGE_DATA_TABLE_LEAVE:
				ParseMessage.onSuccessLogin();
				break;
			case PACKAGE_DATA_TABLE_STATUS:
				ParseMessage.onTableStatus(jsonObject);
				break;
			case PACKAGE_DATA_GAME_READY:
				ParseMessage.onSuccessLogin();
				break;
		}
	}
	
	/**
	 * Metoda wywoływana po otrzymaniu pakietu z błedem.
	 * @throws JSONException 
	 */
	private static void onError() throws JSONException {
		JSONObject jsonObjectData = new JSONObject(jsonObject.getString("data").toString());
		ERROR = Integer.valueOf(jsonObjectData.getString("error"));
		switch(ERROR) {
			case ERROR_WRONG_PACKAGE:
				System.out.println("Nieprawidłowy pakiet.");
				break;
			case ERROR_LOGIN_NOT_AVAILABLE:
				System.out.println("Nick zajety.");
				break;
			case ERROR_NO_FREE_PLACES:
				System.out.println("Brak wolnych miejsc.");
				break;
			case ERROR_LOGIN_FORBIDDEN:
				System.out.println("Niedozwolony nick.");
				break;
			case ERROR_TABLE_NOT_EXIST:
				System.out.println("Stolik nie istnieje.");
				break;
			case ERROR_TABLE_FULL:
				System.out.println("Stolik pełny.");
				break;
			case ERROR_MAX_TABLES:
				System.out.println("Nie można utworzy wiecej stolikow.");
				break;
			case ERROR_NOT_USER_TURN:
				System.out.println("Nie Twój ruch.");
				break;
			case ERROR_WRONG_TURN:
				System.out.println("Nieprawidłowy ruch.");
				break;
		}
	}
	
	/**
	 * Metoda wywoływana po poprawnym zalogowaniu się do gry.
	 */
	private static void onSuccessLogin() {
		MultiplayerSocketGameActivity.showRoomsView();
	}
	
	/**
	 * Metoda aktualizuje liste graczy, którzy są dostępni w grze.
	 * @param jsonObject
	 * @throws JSONException 
	 */
	private static void onPlayerList(JSONObject jsonObject) throws JSONException {
		JSONObject jsonObjectData = new JSONObject(jsonObject.getString("data").toString());
		JSONArray jsonArrayPlayers = new JSONArray(jsonObjectData.getString("players").toString());
		Arrays.playerList.clear();
		for(int i = 0; i < jsonArrayPlayers.length(); i++) {
			JSONObject jsonObjectPlayers = new JSONObject(jsonArrayPlayers.get(i).toString());

            Player player = new Player();
            player.setPlayerID(jsonObjectPlayers.getString("id"));
            player.setPlayerNick(jsonObjectPlayers.getString("nick"));

            Arrays.playerList.add(player);
		}
		MultiplayerSocketGameActivity.updatePlayerList();
	}
	
	/**
	 * Metoda aktualizuje listę użytkowników po dołączeniu, lub opuszczeniu gry.
	 * @param jsonObject
	 * @throws JSONException 
	 */
	private static void onPlayerUpdate(JSONObject jsonObject) throws JSONException {
		JSONObject jsonObjectData = new JSONObject(jsonObject.getString("data").toString());
		Boolean connected = Boolean.valueOf(jsonObjectData.get("connected").toString());
		if(connected == true) {
            Player player = new Player();
            player.setPlayerID(jsonObjectData.getString("id"));
            player.setPlayerNick(jsonObjectData.getString("nick"));

            Arrays.playerList.add(player);
		}else if(connected == false) {
			for(int i = 0; i < Arrays.playerList.size(); i++) {
				if(Arrays.playerList.get(i).getPlayerID().equals(jsonObjectData.getString("id"))) {
					Arrays.playerList.remove(i);
				}
			}
		}
		MultiplayerSocketGameActivity.updatePlayerList();
	}
	
	/**
	 * Metoda aktualizuje liste dostępnych stołów.
	 * @param jsonObject
	 * @throws JSONException 
	 */
	private static void onTableList(JSONObject jsonObject) throws JSONException {
		JSONObject jsonObjectData = new JSONObject(jsonObject.getString("data").toString());
		JSONArray jsonArrayTables = new JSONArray(jsonObjectData.getString("tables").toString());
		Arrays.tableList.clear();
		for(int i = 0; i < jsonArrayTables.length(); i++) {
			JSONObject jsonObjectTables = new JSONObject(jsonArrayTables.get(i).toString());
			JSONObject jsonObjectPlayers = new JSONObject(jsonObjectTables.getString("players").toString());
            Table table = new Table();
            table.setTableID(jsonObjectTables.get("id").toString());
			if(!jsonObjectPlayers.getString("player1").equals("null")) {
                table.setPlayer1("notnull");
				JSONObject jsonObjectPlayerOne = new JSONObject(jsonObjectPlayers.getString("player1").toString());
				for(int z = 0; z < jsonObjectPlayerOne.length(); z++ ) {
                    table.setPlayer1ID(jsonObjectPlayerOne.get("id").toString());
				    table.setPlayer1Nick(jsonObjectPlayerOne.get("nick").toString());
				}
			}else {
				table.setPlayer1("null");
			}
			
			if(!jsonObjectPlayers.getString("player2").equals("null")) {
                table.setPlayer2("notnull");
				JSONObject jsonObjectPlayerOne = new JSONObject(jsonObjectPlayers.getString("player2").toString());
				for(int z = 0; z < jsonObjectPlayerOne.length(); z++ ) {
                    table.setPlayer2ID(jsonObjectPlayerOne.get("id").toString());
                    table.setPlayer2Nick(jsonObjectPlayerOne.get("nick").toString());
				}
			}else {
                table.setPlayer2("null");
			}
			Arrays.tableList.add(table);
		}
		MultiplayerSocketGameActivity.updateTableList();
	}
	
	/**
	 * Metoda aktualizuje liste stołów po opuszczeniu, dołączeniu lub utworzeniu
	 * @param jsonObject
	 * @throws JSONException
	 */
	private static void onTableStatus(JSONObject jsonObject) throws JSONException {
		JSONObject jsonObjectData = new JSONObject(jsonObject.getString("data").toString());
		JSONObject jsonObjectPlayers = new JSONObject(jsonObjectData.getString("players").toString());
		int count = 0;
		if(jsonObjectPlayers.getString("player1").equals("null") && jsonObjectPlayers.getString("player2").equals("null")) {
			for(int i = 0; i < Arrays.tableList.size(); i++) {
				if(Arrays.tableList.get(i).getTableID().toString().equals(jsonObjectData.getString("id"))) {
					Arrays.tableList.remove(i);
				}
			}
			
		}else {
			for(int i = 0; i < Arrays.tableList.size(); i++) {
				if(Arrays.tableList.get(i).getTableID().toString().equals(jsonObjectData.getString("id"))) {
					Arrays.tableList.remove(i);
					addTableToList(jsonObjectData, jsonObjectPlayers);
					count = 1;
				}
			}
			if(count == 0) {
				addTableToList(jsonObjectData, jsonObjectPlayers);
			}
		}
		MultiplayerSocketGameActivity.updateTableList();
	}
	
	/**
	 * Dodawanie stołów do listy.
	 * @param jsonObjectData
	 * @param jsonObjectPlayers
	 * @throws JSONException
	 */
	private static void addTableToList(JSONObject jsonObjectData, JSONObject jsonObjectPlayers) throws JSONException {
        Table table = new Table();
        table.setTableID(jsonObjectData.get("id").toString());
		if(!jsonObjectPlayers.getString("player1").equals("null")) {
            table.setPlayer1("notnull");
			JSONObject jsonObjectPlayerOne = new JSONObject(jsonObjectPlayers.getString("player1").toString());
			for(int z = 0; z < jsonObjectPlayerOne.length(); z++ ) {
                table.setPlayer1ID(jsonObjectPlayerOne.get("id").toString());
                table.setPlayer1Nick(jsonObjectPlayerOne.get("nick").toString());
			}
		}else {
            table.setPlayer1("null");
		}
		
		if(!jsonObjectPlayers.getString("player2").equals("null")) {
            table.setPlayer2("notnull");
			JSONObject jsonObjectPlayerOne = new JSONObject(jsonObjectPlayers.getString("player2").toString());
			for(int z = 0; z < jsonObjectPlayerOne.length(); z++ ) {
                table.setPlayer2ID(jsonObjectPlayerOne.get("id").toString());
                table.setPlayer2Nick(jsonObjectPlayerOne.get("nick").toString());
			}
		}else {
            table.setPlayer2("null");
		}
		Arrays.tableList.add(table);
	}

    /**
     * Dołączenie do stolika.
     * @param jsonObject
     * @throws JSONException
     */
    public static void onTableJoin(JSONObject jsonObject) throws JSONException {
        JSONObject jsonObjectData = new JSONObject(jsonObject.getString("data").toString());
        //Player.playerOne    = Boolean.valueOf(jsonObjectData.getString("player1").toString());
        //Player.tableID      = Integer.valueOf(jsonObjectData.getString("table").toString());
        MultiplayerSocketGameActivity.showGameView();
    }
}
