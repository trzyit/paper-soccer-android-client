package com.trzy.it.papersoccer.multiplayer.socket.game.adapters;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.trzy.it.papersoccer.R;
import com.trzy.it.papersoccer.multiplayer.socket.game.model.Player;

public class PlayerListAdapter extends BaseAdapter {

	private List<Player> playerList;
	private static LayoutInflater inflater = null;

	public PlayerListAdapter(Activity activity, List<Player> playerList) {
        this.playerList = playerList;
		inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public int getCount() {
		return playerList.size();
	}

	public String getItem(int position) {
		return "";
	}

	static class ViewHolder {
		public TextView text;
	}

	public View getView(int position, View view, ViewGroup parent) {

		View playerListRow = view;
		final ViewHolder holder;

		if (playerListRow == null) {
			playerListRow = inflater.inflate(R.layout.player_list_row_view, null);
			holder = new ViewHolder();
			holder.text = (TextView) playerListRow.findViewById(R.id.player_name);
			playerListRow.setTag(holder);
		} else {
			holder = (ViewHolder) playerListRow.getTag();
		}
		
		holder.text.setText(playerList.get(position).getPlayerNick().toString());
		return playerListRow;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}
}