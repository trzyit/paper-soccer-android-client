package com.trzy.it.papersoccer.multiplayer.socket.game.model;

/**
 *
 * Created by Dawid Sajdak <dawid@trzy.it> on 15:39 - 12.07.13.
 */
public class Table {

    /**
     * @var String
     */
    protected String tableID;

    /**
     * @var String
     */
    protected String player1;

    /**
     * @var String
     */
    protected String player1ID;

    /**
     * @var String
     */
    protected String player1Nick;

    /**
     * @var String
     */
    protected String player2;

    /**
     * @var String
     */
    protected String player2ID;

    /**
     * @var String
     */
    protected String player2Nick;

    public String getTableID() {
        return player1;
    }

    public void setTableID(String tableID) {
        this.tableID = tableID;
    }

    public String getPlayer1() {
        return player1;
    }

    public void setPlayer1(String player1) {
        this.player1 = player1;
    }

    public String getPlayer1ID() {
        return player1ID;
    }

    public void setPlayer1ID(String player1ID) {
        this.player1ID = player1ID;
    }

    public String getPlayer1Nick() {
        return player1Nick;
    }

    public void setPlayer1Nick(String player1Nick) {
        this.player1Nick = player1Nick;
    }

    public String getPlayer2() {
        return player2;
    }

    public void setPlayer2(String player2) {
        this.player2 = player2;
    }

    public String getPlayer2ID() {
        return player2ID;
    }

    public void setPlayer2ID(String player2ID) {
        this.player2ID = player2ID;
    }

    public String getPlayer2Nick() {
        return player2Nick;
    }

    public void setPlayer2Nick(String player2Nick) {
        this.player2Nick = player2Nick;
    }
}
