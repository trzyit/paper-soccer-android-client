package com.trzy.it.papersoccer.multiplayer.socket.game.model;

/**
 *
 * Created by Dawid Sajdak <dawid@trzy.it> on 15:36 - 12.07.13.
 */
public class Player {

    /**
     * @var Integer
     */
    protected String playerID;

    /**
     * @var String
     */
    protected String playerNick;

    public String getPlayerID() {
        return playerID;
    }

    public void setPlayerID(String playerID) {
        this.playerID = playerID;
    }

    public String getPlayerNick() {
        return playerNick;
    }

    public void setPlayerNick(String playerNick) {
        this.playerNick = playerNick;
    }
}
