package com.trzy.it.papersoccer.multiplayer.socket.game.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import com.trzy.it.papersoccer.MultiplayerSocketGameActivity;
import com.trzy.it.papersoccer.R;
import com.trzy.it.papersoccer.multiplayer.socket.game.model.Table;

import java.util.List;

public class TableListAdapter extends BaseAdapter {

	private List<Table> tableList;
	private static LayoutInflater inflater = null;

	public TableListAdapter(Activity activity, List<Table> tableList) {
		this.tableList = tableList;
		inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public int getCount() {
		return tableList.size();
	}

	public String getItem(int position) {
		return "";
	}

	static class ViewHolder {
		public TextView player1Nick;
		public TextView player2Nick;
		
		public Button player1Button;
		public Button player2Button;
	}

	public View getView(final int position, View view, ViewGroup parent) {
		View tableListRow = view;
		final ViewHolder holder;

		if (tableListRow == null) {
			tableListRow = inflater.inflate(R.layout.table_list_row_view, null);
			holder = new ViewHolder();
			holder.player1Nick = (TextView) tableListRow.findViewById(R.id.player1_name);
			holder.player2Nick = (TextView) tableListRow.findViewById(R.id.player2_name);
			
			holder.player1Button = (Button) tableListRow.findViewById(R.id.button_connect_player1);
			holder.player2Button = (Button) tableListRow.findViewById(R.id.button_connect_player2);
			tableListRow.setTag(holder);
		} else {
			holder = (ViewHolder) tableListRow.getTag();
		}
		if(tableList.get(position).getPlayer1().equals("null")) {
			holder.player1Button.setVisibility(View.VISIBLE);
			holder.player1Nick.setVisibility(View.INVISIBLE);
		}else {
			holder.player1Button.setVisibility(View.INVISIBLE);
			holder.player1Nick.setVisibility(View.VISIBLE);
			holder.player1Nick.setText(tableList.get(position).getPlayer1Nick().toString());
		}
		
		if(tableList.get(position).getPlayer2().equals("null")) {
			holder.player2Button.setVisibility(View.VISIBLE);
			holder.player2Nick.setVisibility(View.INVISIBLE);
		}else {
			holder.player2Button.setVisibility(View.INVISIBLE);
			holder.player2Nick.setVisibility(View.VISIBLE);
			holder.player2Nick.setText(tableList.get(position).getPlayer2Nick().toString());
		}

        holder.player1Button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                MultiplayerSocketGameActivity.joinToTable(Integer.valueOf(tableList.get(position).getTableID().toString()));
            }
        });
        holder.player2Button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                MultiplayerSocketGameActivity.joinToTable(Integer.valueOf(tableList.get(position).getTableID().toString()));
            }
        });
        return tableListRow;
    }

	@Override
	public long getItemId(int position) {
		return 0;
	}
}