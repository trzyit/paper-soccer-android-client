package com.trzy.it.papersoccer;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity implements OnClickListener {

	Button networkGame;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		networkGame = (Button) findViewById(R.id.networkGame);
		networkGame.setOnClickListener(this);
	}
	
	/**
	 * Metoda włacza gre multiplayer via sockets.
	 */
	void networkGameClicked() {
		Intent i = new Intent(getApplicationContext(), MultiplayerSocketGameActivity.class);
		startActivity(i);
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()) {
			case R.id.networkGame:
				networkGameClicked();
				break;
		}
	}
}
