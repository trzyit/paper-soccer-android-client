package com.trzy.it.papersoccer;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import io.socket.IOAcknowledge;
import io.socket.IOCallback;
import io.socket.SocketIO;
import io.socket.SocketIOException;

import java.net.MalformedURLException;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ViewFlipper;

import com.trzy.it.papersoccer.multiplayer.socket.game.Arrays;
import com.trzy.it.papersoccer.multiplayer.socket.game.Player;
import com.trzy.it.papersoccer.multiplayer.socket.game.adapters.PlayerListAdapter;
import com.trzy.it.papersoccer.multiplayer.socket.game.adapters.TableListAdapter;
import com.trzy.it.papersoccer.protocol.ParseMessage;

public class MultiplayerSocketGameActivity extends Activity implements IOCallback {

	private static SocketIO socket;
	private static Handler mHandler = new Handler();

    private static Context context;

	Button loginButton;
	EditText loginInput;
	static ListView playerList;
	static ListView tableList;
	static ViewFlipper viewFlipper;
	
	static PlayerListAdapter playerListAdapter; 
	static TableListAdapter tableListAdapter; 
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.multiplayer_socket);

        MultiplayerSocketGameActivity.context = getApplicationContext();

		//Przypisanie widoków
		loginButton = (Button) findViewById(R.id.login_button);
		loginInput = (EditText) findViewById(R.id.login_input);
		
		playerList = (ListView) findViewById(R.id.player_list);
		tableList = (ListView) findViewById(R.id.table_list);
		viewFlipper = (ViewFlipper) findViewById(R.id.viewFlipper);

		//Przypisanie adapterów
		playerListAdapter = new PlayerListAdapter(MultiplayerSocketGameActivity.this, Arrays.playerList);
		tableListAdapter = new TableListAdapter(MultiplayerSocketGameActivity.this, Arrays.tableList);
		
		//Przypisanie buttonów do listnerów.
		loginButton.setOnClickListener(loginButtonListner);
		
		try {
			this.makeConnection();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public void onStart() {
		super.onStart();
		playerList.setAdapter(playerListAdapter);
		tableList.setAdapter(tableListAdapter);
	}

	/**
	 * Listner obsługujący logowanie usera do serwera.
	 */
	OnClickListener loginButtonListner = new OnClickListener() {
		@Override
		public void onClick(View v) {
			socket.send("{\"type\":1,\"data\": {\"nick\":\"" + loginInput.getText().toString() + "\"}}");
		}
	};
	
	/**
	 * Metoda nazwiązuje połączenie z serwerem.
	 */
	private void makeConnection() throws MalformedURLException, JSONException {
		socket = new SocketIO();
		socket.connect("http://5.187.48.69:5421", this);
	}

    /**
     * Metoda laczy uzytkownika do stolika.
     */
    public static void joinToTable(Integer tableID) {
        socket.send("{\"type\":2,\"data\": {\"table\":\"" + tableID + "\"}}");
    }

	/**
	 * Metoda wyświetla widok logowania do serwera.
	 */
	private static void showLoginView() {
		mHandler.postDelayed(runnableLogin, 1);
	}

	/**
	 * Metoda wyświetla liste pokoi oraz liste graczy aktualnie dostępnych w
	 * grze.
	 */
	public static void showRoomsView() {
		mHandler.postDelayed(runnableRooms, 1);
		MultiplayerSocketGameActivity.updatePlayerList();
		MultiplayerSocketGameActivity.updateTableList();
	}

    public static void showGameView() {
        mHandler.postDelayed(runnableGameView, 1);
    }
	
	/**
	 * Metoda aktualizuje liste graczy.
	 */
	public static void updatePlayerList() {
		if(viewFlipper.getDisplayedChild() == 1) {
			mHandler.postDelayed(updatePlayerList, 1);
		}
	}

    /**
     * Metoda aktualizuje liste pokoi.
     */
	public static void updateTableList() {
		if(viewFlipper.getDisplayedChild() == 1) {
			mHandler.postDelayed(updateTableList, 1);
		}
	}

	/**
	 * Zmiana widoku na formularz logowania.
	 */
	private static Runnable runnableLogin = new Runnable() {
	    public void run() {
	    	viewFlipper.setDisplayedChild(0);
	    }
	};
	
	/**
	 * Aktualizowanie listy graczy online
	 */
	private static Runnable updatePlayerList = new Runnable() {
	    public void run() {
	    	playerListAdapter.notifyDataSetChanged();
	    }
	};
	
	/**
	 * Zmiana widoku na liste stołów oraz liste użytkowników.
	 */
	private static Runnable runnableRooms = new Runnable() {
	    public void run() {
	    	viewFlipper.setDisplayedChild(1);
	    }
	};
	
	/**
	 * Aktualizowanie listy pokoi online
	 */
	private static Runnable updateTableList = new Runnable() {
	    public void run() {
	    	tableListAdapter.notifyDataSetChanged();
	    }
	};

    private static Runnable runnableGameView = new Runnable() {
        public void run() {
            viewFlipper.setDisplayedChild(2);
            AlertDialog.Builder builder = new AlertDialog.Builder(MultiplayerSocketGameActivity.context);
            builder.setMessage("Are you ready?")
                    .setPositiveButton("Nieeeeee", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // FIRE ZE MISSILES!
                        }
                    })
                    .setNegativeButton("Tak!! Dawać to!", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User cancelled the dialog
                        }
                    });
            // Create the AlertDialog object and return it
            builder.create();
            builder.show();
        }
    };
	
	@Override
	public void on(String arg0, IOAcknowledge arg1, Object... arg2) {
		System.out.println("on");
	}

	@Override
	public void onConnect() {
		System.out.println("onConnect");
        MultiplayerSocketGameActivity.showLoginView();
	}

	@Override
	public void onDisconnect() {
		Intent i = new Intent(getApplicationContext(), MainActivity.class);
		startActivity(i);
	}

	@Override
	public void onError(SocketIOException socketIOException) {
		socketIOException.printStackTrace();
		Intent i = new Intent(getApplicationContext(), MainActivity.class);
		startActivity(i);
	}

	@Override
	public void onMessage(String message, IOAcknowledge arg1) {
		ParseMessage.message = message;
		try {
			ParseMessage.parse();
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onMessage(JSONObject arg0, IOAcknowledge arg1) {
		System.out.println("onMessage1" + arg0);
	}

	@Override
	public void onStop() {
		super.onStop();
		socket.disconnect();
	}

	@Override
	public void onPause() {
		super.onPause();
		socket.disconnect();
	}
}
